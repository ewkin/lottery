import React from "react";
import './App.css';
import Lottery from "./Lottery/Lottery";

class App extends React.Component {

    state = {
        lottery: [
            {number: 0, numberShow: false},
            {number: 0, numberShow: false},
            {number: 0, numberShow: false},
            {number: 0, numberShow: false},
            {number: 0, numberShow: false}
        ]
    };

    getRandomNumbers = () => {

        let numberSet = new Set();
        let rand;
        while(numberSet.size < 5){
            rand = Math.floor(5 + Math.random() * (36 + 1 - 5));
            numberSet.add(rand);
        }
        let numbers = [...numberSet];


        const lottery = this.state.lottery.map((val, i) =>{
            return {
                number: numbers[i],
                numberShow: true

            };
        });
        this.setState({lottery});
    };

    render() {
        return (
            <div className="App">
                <button onClick={this.getRandomNumbers} className ="mb-4 m-auto d-block btn btn-primary">
                    New numbers
                </button>
                {this.state.lottery[0].numberShow ? <Lottery number = {this.state.lottery[0].number} /> : null}
                {this.state.lottery[1].numberShow ? <Lottery number = {this.state.lottery[1].number} /> : null}
                {this.state.lottery[2].numberShow ? <Lottery number = {this.state.lottery[2].number} /> : null}
                {this.state.lottery[3].numberShow ? <Lottery number = {this.state.lottery[3].number} /> : null}
                {this.state.lottery[4].numberShow ? <Lottery number = {this.state.lottery[4].number} /> : null}
            </div>
        );
    }
}

export default App;
