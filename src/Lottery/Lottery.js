import React from 'react';

const Lottery = props =>{
    return (
        <div className="lottery mr-2 m-auto d-inline-block rounded-circle bg-success text-white text-center pt-2"  style={{width: 44 + 'px', height: 44 +'px'}} >
            {props.number}
        </div>
    )
};

export default Lottery;